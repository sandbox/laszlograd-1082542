<?php

/**
 * @file
 * Provides the HelloCaptcha administration settings.
 */

/**
 * Form callback; administrative settings for HelloCaptcha.
 */
function hellocaptcha_admin_settings() {
  // Load the hellocaptcha library.
  _hellocaptcha_load_library();

  $form = array();
  $form['hellocaptcha_profileid'] = array(
    '#type' => 'textfield',
    '#title' => t('Profileid'),
    '#default_value' => variable_get('hellocaptcha_profileid', HELLOCAPTCHA_TRIAL_PROFILE_ID),
    '#maxlength' => 24,
    '#description' => t('HelloCaptcha requires a profileid. You can sign up for a FREE HelloCaptcha profileid here: <a href="@url" target="_blank">www.hellocaptcha.com/register</a>.', array('@url' => 'http://www.hellocaptcha.com/register/')),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Validation function for hellocaptcha_admin_settings().
 *
 * @see hellocaptcha_admin_settings()
 */
function hellocaptcha_admin_settings_validate($form, &$form_state) {
}

