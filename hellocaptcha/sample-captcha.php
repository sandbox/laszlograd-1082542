<?php
/**
  * This file contains sample PHP code that demonstrates how to use
  * the HelloCaptcha class.
  *
  * The most recent version of this file can be downloaded from:
  * http://www.hellocaptcha.com/dl/sample-captcha.php
  *
  * This sample includes the HelloCaptcha.class.php file that can be
  * downloaded from:
  * http://www.hellocaptcha.com/dl/HelloCaptcha.class.php
  *
  * PHP versions before 5.2 do not have JSON support enabled. To solve
  * this issue download JSON.php from our server and place it near the
  * HelloCaptcha.class.php file. The HelloCaptcha.class.php file
  * automatically includes it if neccessary. The file can be downloaded
  * from:
  * http://www.hellocaptcha.com/dl/JSON.php
  * 
  * @see http://www.hellocaptcha.com
  * @see http://www.hellocaptcha.com/doc/embedding
  * @Copyright Program Produkt, 2010-2011
  */
?>
<html>
  <head>
    <title>sample CAPTCHA - HelloCaptcha</title>
  <head>
  <body>
<?php
// Include HelloCaptcha class
require_once('HelloCaptcha.class.php');

// Set profileid. Profile id can be obtained after registration on www.hellocaptcha.com.
// The registration can be found on the login panel. Registered users can create profiles,
// where CAPTCHAs can be customized.
$profileid = HELLOCAPTCHA_TRIAL_PROFILE_ID;

// If there was a user response and we have the Turing test id.
$wasthereuserresponse = isset( $_POST['hellocaptcha_turingtestid'] ) && isset( $_POST['hellocaptcha_user_answer'] ); 

// If there was user response then print the test result 
if ( $wasthereuserresponse ) {

  $turingtestid = $_POST['hellocaptcha_turingtestid'];
  $useranswer = $_POST['hellocaptcha_user_answer'];

  // Ignoring empty answers.
  if ( empty( $turingtestid ) || empty( $useranswer ) ) {
    echo 'Spam submission!<br />';

  // Check answer and print test result
  } else {
    $resp = HelloCaptcha::checkAnswer( $turingtestid, $useranswer );
    if ( $resp ) {
      echo 'You got it!<br />';
    } else {
      echo 'Bad answer<br />';
    }
  }

// If there is no user response then print the CAPTCHA  
} else {
  echo '<form action="sample-captcha.php" method="post">';
  echo HelloCaptcha::getEmbedCode( $profileid );
  echo '<input type="submit" value="submit" />';
  echo '</form>';
}

?>
  <a href="sample-captcha.php">Take a new CAPTCHA</a>
  </body>
</html>